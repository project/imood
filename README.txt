name:    imood module
license: GPL
by:      Breyten Ernsting (bje@dds.nl)

requirements: Drupal 4.6.3 or above, profile module needs to be enabled.

installation: copy files to your modules directory, activate the module via admin/modules , and go to the imood settings page to specify which profile fields are for use with imood. Note that the field for 'base mood' needs to be a selection list, and that it's options will be periodically overwritten as the possible mood list will be downloaded from imood. The personal mood field can just be a textfield.

That's all.
